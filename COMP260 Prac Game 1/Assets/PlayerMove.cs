﻿using UnityEngine;
using System.Collections;

public class PlayerMove : MonoBehaviour {
public Vector2 move;
public Vector2 velocity; // in metres per second
public float maxSpeed = 5.0f;
	public string horizontalAxis;
	public string verticalAxis;
	void start() {
	}

	void Update() {
		// get the input values
		Vector2 direction;
		direction.x = Input.GetAxis(horizontalAxis);
		direction.y = Input.GetAxis(verticalAxis);

		// scale by the maxSpeed parameter
		Vector2 velocity = direction * maxSpeed;

		// move the object
		transform.Translate(velocity * Time.deltaTime);
	}

}