﻿using UnityEngine;
using System.Collections;

public class BeeMove : MonoBehaviour {

	public float speed = 4.0f;        // metres per second
	public float turnSpeed = 180.0f;  // degrees per second
	public Transform target;
	public Transform target1;
	public Transform target2;
	private Vector2 heading = Vector2.right; 

	void Update() {


		// get the vector from the bee to the target 
		Vector2 direction1 = target1.position - transform.position;
		Vector2 direction2 = target2.position - transform.position;
		if (direction1.magnitude < direction2.magnitude) {
			target = target1;
		} else {
			target = target2;
		}
		// calculate how much to turn per frame
		float angle = turnSpeed * Time.deltaTime;
		Vector2 direction = target.position - transform.position;
		// turn left or right
		if (direction.IsOnLeft(heading)) {
			// target on left, rotate anticlockwise
			heading = heading.Rotate(angle);
		}
		else {
			// target on right, rotate clockwise
			heading = heading.Rotate(-angle);
		}

		transform.Translate(heading * speed * Time.deltaTime);
	}

	void OnDrawGizmos() {
		// draw heading vector in red
		Gizmos.color = Color.red;
		Gizmos.DrawRay(transform.position, heading);

		// draw target vector in yellow
		Gizmos.color = Color.yellow;
		Vector2 direction = target.position - transform.position;
		Gizmos.DrawRay(transform.position, direction);
	}

}